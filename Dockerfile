FROM debian:stretch
MAINTAINER Niels Jansen, niels@nielsj.net

# install ruby dependencies
RUN apt-get update && apt-get install -y \
      python procps && apt-get clean

RUN mkdir /LasaurApp
COPY . /LasaurApp

WORKDIR /LasaurApp

EXPOSE 4444

ENTRYPOINT python backend/app.py -p
#run.sh
#python backend/app.py

